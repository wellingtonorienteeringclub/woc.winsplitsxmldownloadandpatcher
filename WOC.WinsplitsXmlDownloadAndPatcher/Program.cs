﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Flurl.Http;
using Flurl.Http.Xml;
using IOF.XML.V3;
using Newtonsoft.Json;

namespace ConsoleApp1 {
  class Program {
    static async Task Main(string[] args) {
      while (true) {
        Console.WriteLine("Enter Winsplit Id or x to quit:");
        string id = Console.ReadLine();

        if (id == "x") {
          break;
        }

        if (Int32.TryParse(id, out int idAsInt)) {
          string url = $"http://obasen.orientering.se/winsplits/wsp4/communication/server.asp?action=downloadEvent&databaseId={id}";
          var rawFile = await url.GetBytesAsync();
          var people = ParseFile(rawFile);

          url = $"http://obasen.orientering.se/winsplits/api/events/{id}/resultlist/xml";

          var xmlFile = await url.GetXmlAsync<ResultList>();

          var lookup = people.ToDictionary(k => $"{k.FirstName} {k.Lastname} {k?.Grade?.Name} {k.Club}");

          foreach (var classResult in xmlFile.ClassResult) {
            foreach (var personResult in classResult.PersonResult) {
              foreach (var result in personResult.Result) {
                string key = $"{personResult?.Person?.Name?.Given} {personResult?.Person?.Name?.Family} {classResult?.Class.Name} {personResult?.Organisation?.Name}";

                if (lookup.ContainsKey(key)) {
                  result.ControlCard = new List<ControlCard> { new ControlCard { Value = lookup[key].Card } }.ToArray();
                }
              }
            }
          }

          using (TextWriter writer = new StreamWriter($"{id}.xml")) {
            new XmlSerializer(typeof(ResultList)).Serialize(writer, xmlFile);
          }

          await File.WriteAllTextAsync($"{id}.json", JsonConvert.SerializeObject(people, Formatting.Indented));
          Console.WriteLine($"Saved file as {id}.json");
        }
      }

      Console.WriteLine("Bye");
      Console.ReadLine();
    }

    static List<Person> ParseFile(byte[] rawFile) {
      var result = new List<Person>();

      var classes = SplitArray(rawFile, (b1, b2, b3, data, index, last) => {
        if (b1 == 0x43 && b2 != 0x00 && b3 == 0x00) {
          if (last) {
            return true;
          }

          int length = b2;
          int endFirst = index + length + 1;
          if (data[endFirst] != 0x44) {
            return false;
          }

          int length2 = data[endFirst + 1];
          int end2 = endFirst + length2 + 3;

          if (data[end2] != 0x45) {
            return false;
          }

          return true;
        }

        return false;
      });

      foreach (var c in classes) {
        var buffers = SplitArray(c.ToArray(), (b1, b2, b3, data, index, last) => {
          return b1 == 0x00 && b2 == 0x00 && b3 == 0x84;
        });

        var className = ParseClass(c.ToArray());

        var people = buffers.Select(p => ParsePerson(p.ToArray())).ToList();

        foreach (var person in people) {
          person.Grade = new Grade { Name = className };
        }

        result.AddRange(people);
      }

      return result;
    }

    static List<List<byte>> SplitArray(byte[] data, Func<byte, byte, byte, byte[], int, bool, bool> func) {

      var buffers = new List<List<byte>>();
      var buffer = new List<byte>();
      int previousIndex = 0;

      for (var i = 3; i < data.Length; i++) {
        var b1 = data[i - 2];
        var b2 = data[i - 1];
        var b3 = data[i];

        if (func(b1, b2, b3, data, i, false)) {
          if (buffer.Count > 3) {
            if (func(buffer[0], buffer[1], buffer[2], data, previousIndex, false)) {
              buffers.Add(buffer);
            }
            previousIndex = i;
            buffer = new List<byte>();
          }
        }

        buffer.Add(b1);
      }

      if (buffer.Count > 3 && func(buffer[0], buffer[1], buffer[2], data, previousIndex, true)) {
        buffers.Add(buffer);
      }

      return buffers;
    }

    public static string ParseClass(byte[] data) {
      if (data == null && data.Length < 2) {
        return null;
      }

      int length = data[1];

      if (data.Length < length + 2) {
        return null;
      }

      return Encoding.UTF8.GetString(data, 3, length);
    }


    public static Person ParsePerson(byte[] data) {
      var person = new Person();

      for (var i = 2; i < data.Length; i++) {
        var p2 = data[i - 2];
        var p1 = data[i - 1];
        var b = data[i];


        if (b == 0x84 && p1 == 0x00 && p2 == 0x00 && i + 3 < data.Length) {
          var card = new List<byte> { data[i + 1], data[i + 2], data[i + 3], 0x00 }.ToArray();
          person.Card = BitConverter.ToInt32(card, 0).ToString();
        } else if (b == 0x87 && person.FirstName == null && i > 6) {
          person.FirstName = GetString(data, i);
        } else if (b == 0x88 && person.Lastname == null && i > 6) {
          person.Lastname = GetString(data, i);
        } else if (b == 0x8C && person.Club == null && i > 6) {
          person.Club = GetString(data, i);
        }
      }

      return person;
    }

    private static string GetString(byte[] data, int i) {
      if (i + 3 < data.Length && data[i + 2] == 0x00) {
        int length = data[i + 1];
        if (i + length + 1 < data.Length) {
          return Encoding.UTF8.GetString(data, i + 3, length);
        }
      }

      return null;
    }
  }

  public class Person {
    public string Card { get; set; }
    public string FirstName { get; set; }
    public string Lastname { get; set; }
    public string Club { get; set; }
    public Grade Grade { get; set; }
  }

  public class Grade {
    public string Name { get; set; }
  }
}
